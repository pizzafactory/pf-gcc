/* Specify predefined symbols in preprocessor.  */

#define TARGET_OS_CPP_BUILTINS()	\
do {					\
  builtin_define ("__toppers__");	\
} while (0)

/* No sdata.
 * The TOPPERS BSPs expect -G0
 */
#undef MIPS_DEFAULT_GVALUE
#define MIPS_DEFAULT_GVALUE 0

